MCU ?= atmega328p

CC = avr-gcc
OBJCOPY = avr-objcopy
#CFLAGS = -g -mmcu=$(MCU) -Wall -Wextra -Werror=return-type -pedantic -Wstrict-prototypes -O3 -mcall-prologues
CFLAGS = -g -mmcu=$(MCU) -Wall -Wextra -pedantic -Wstrict-prototypes -O3 -mcall-prologues

new: clean all

all: main.hex

main.hex: main.elf
	$(OBJCOPY) -R .eeprom -O ihex $< $@
main.elf: main.o
	$(CC) $(CFLAGS) -o $@ $^
%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf *.o *.elf *.hex

flash: main.hex
	#avrdude -P /dev/ttyACM0 -U flash:w:main.hex:i -c wiring -p $(MCU)
	avrdude -P /dev/ttyUSB0  -b 57600 -U flash:w:main.hex:i -c arduino -p $(MCU)

.PHONY: clean
.SUFFIXES:
