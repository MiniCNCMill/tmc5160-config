//#define MCU_atmega2560
#define MCU_atmega328p

#include <avr/io.h>
#include "tmc/ic/TMC5160/TMC5160.c"
#include <stdio.h>


//#include "register_definitions.h"

/*
 * SPI Pins:
 *
 *
 *			Atmega2560		atmega328p
 *			AVR|Arduino			AVR|Arduino
 * MISO		PB3	50				PB4	D12
 * MOSI		PB2	51				PB3	D11
 * SCK		PB1	52				PB5	D13
 * SS		PB0	53				PB2	D10		<<<<<<<<<<< erstmal abgeschaltet
 *	
 * SS-X		PE4					PD2	D2
 * SS-Y		PE5					PD3	D3
 * SS-Z		PG5					PD4	D4
 * STEP-X	-					PD5 D5
 * STEP-Y	-					PD6 D6
 * STEP-Z	-					PD7 D7
 * DIR-X	-					PB0 D8
 * DIR-Y	-					PB1 D9
 * DIR-Z	-					PB2 D10
 */

#ifdef MCU_atmega2560

#define MCU atmega2560
#define F_CPU 16000000UL

// CS X-Achse, PE4, Arduino PWM 2
#define TMC_CS_X_DDR	DDRE
#define TMC_CS_X_PORT	PORTE
#define TMC_CS_X_PIN	PE4

// CS Y-Achse, PE5, Arduino PWM 3
#define TMC_CS_Y_DDR	DDRE
#define TMC_CS_Y_PORT	PORTE
#define TMC_CS_Y_PIN	PE5

// CS Z-Achse, PG5, Arduino PWM 4
#define TMC_CS_Z_DDR	DDRG
#define TMC_CS_Z_PORT	PORTG
#define TMC_CS_Z_PIN	PG5

#define SS_DDR		DDRB
#define SS_PIN		PB0

#define MOSI_DDR	DDRB
#define MOSI_PIN	PB2

#define SCK_DDR		DDRB
#define SCK_PIN		PB1

#endif

#ifdef MCU_atmega328p

#define MCU atmega328p
#define F_CPU 16000000UL

/*--------------------------------*/
// CS X-Achse, PD2, Arduino D2
#define TMC_CS_X_DDR	DDRD
#define TMC_CS_X_PORT	PORTD
#define TMC_CS_X_PIN	PD2

// CS Y-Achse, PD3, Arduino D3
#define TMC_CS_Y_DDR	DDRD
#define TMC_CS_Y_PORT	PORTD
#define TMC_CS_Y_PIN	PD3

// CS Z-Achse, PD4, Arduino D4
#define TMC_CS_Z_DDR	DDRD
#define TMC_CS_Z_PORT	PORTD
#define TMC_CS_Z_PIN	PD4

/*--------------------------------*/
// STEP X-Achse, PD5, D5
#define TMC_STEP_X_DDR	DDRD
#define TMC_STEP_X_PORT	PORTD
#define TMC_STEP_X_PIN	PD5

// STEP Y-Achse, PD6, D6
#define TMC_STEP_Y_DDR	DDRD
#define TMC_STEP_Y_PORT	PORTD
#define TMC_STEP_Y_PIN	PD6

// STEP Z-Achse, PD7, D7
#define TMC_STEP_Z_DDR	DDRD
#define TMC_STEP_Z_PORT	PORTD
#define TMC_STEP_Z_PIN	PD7

/*--------------------------------*/
// DIR X-Achse, PB0, D8
#define TMC_DIR_X_DDR	DDRB
#define TMC_DIR_X_PORT	PORTB
#define TMC_DIR_X_PIN	PB0

// DIR Y-Achse, PB1, D9
#define TMC_DIR_Y_DDR	DDRB
#define TMC_DIR_Y_PORT	PORTB
#define TMC_DIR_Y_PIN	PB1

// DIR Z-Achse, PB2, D10
#define TMC_DIR_Z_DDR	DDRB
#define TMC_DIR_Z_PORT	PORTB
#define TMC_DIR_Z_PIN	PB2

//#define SS_DDR		DDRB
//#define SS_PIN		PB2

#define MOSI_DDR	DDRB
#define MOSI_PIN	PB3

#define SCK_DDR		DDRB
#define SCK_PIN		PB5

#endif

// include here because it must come after cpu clock definition
#include <util/delay.h>

enum TMC_ID {
	none = 255,
	x = 0,
	y = 1,
	z = 2,
};

uint8_t spi_status_reg = 0x00;

void TMC_setup_chip_SPI(enum TMC_ID chip) {
    // Deassert all chips by pulling their CS line to HIGH:
    TMC_CS_X_PORT |= (1<<TMC_CS_X_PIN);
    TMC_CS_Y_PORT |= (1<<TMC_CS_Y_PIN);
    TMC_CS_Z_PORT |= (1<<TMC_CS_Z_PIN);

    if(chip == none) {
        // we're done here, nothing to pull down
        return ;
    }

    // give CS lines a tad of time to settle HIGH: 
    _delay_us(10);

    switch (chip){
        // Depending on the chip, assert their CS line by pulling it LOW:
        case x: TMC_CS_X_PORT &= ~(1<<TMC_CS_X_PIN); break;
        case y: TMC_CS_Y_PORT &= ~(1<<TMC_CS_Y_PIN); break;
        case z: TMC_CS_Z_PORT &= ~(1<<TMC_CS_Z_PIN); break;
		case none: return;
    }

    // give CS line a tad of time to settle LOW: 
    _delay_us(10);
}

// => SPI wrapper
// Send [length] bytes stored in the [data] array over SPI and overwrite [data]
// with the reply. The first byte sent/received is data[0].
void tmc5160_readWriteArray(uint8_t channel, uint8_t *data, size_t length){
	TMC_setup_chip_SPI(channel);

	for(uint8_t i = 0; i < length; i++){
		SPDR = data[i];
		while (!(SPSR & (1<<SPIF)));
		data[i] = SPDR;
	}

	TMC_setup_chip_SPI(none);
}

void STEP_DIR_PIN_setup(void)
{
	TMC_STEP_X_DDR |= 0<<TMC_STEP_X_PIN;
	TMC_STEP_Y_DDR |= 0<<TMC_STEP_Y_PIN;
	TMC_STEP_Z_DDR |= 0<<TMC_STEP_Z_PIN;
	TMC_DIR_X_DDR |= 0<<TMC_DIR_X_PIN;
	TMC_DIR_Y_DDR |= 0<<TMC_DIR_Y_PIN;
	TMC_DIR_Z_DDR |= 0<<TMC_DIR_Z_PIN;
}

void SPI_setup(void){
	//SS_DDR |= 1<<SS_PIN;
	MOSI_DDR |= 1<<MOSI_PIN;
	SCK_DDR |= 1<<SCK_PIN;
	//DDRB |= 1<<PB0 | 1<<PB1 | 1<<PB2; 
	SPCR = 1<<SPE | 1<<MSTR | 1<<CPOL | 1<<CPHA | 1<<SPR0 | 1<<SPR1; // enable, master, idle high, leading setup, 125kBit (F_CPU/128)
	TMC_CS_X_DDR |= 1 << TMC_CS_X_PIN;
	TMC_CS_Y_DDR |= 1 << TMC_CS_Y_PIN;
	TMC_CS_Z_DDR |= 1 << TMC_CS_Z_PIN;
	TMC_setup_chip_SPI(none);
}

uint32_t SPI_transceive_reg(enum TMC_ID chip, uint8_t Addr, uint32_t data){
	uint8_t status;
	uint32_t ret = 0x00000000;
	uint8_t temp;

	TMC_setup_chip_SPI(chip);
	
	SPDR = Addr;
	while (!(SPSR & (1<<SPIF)));
	status = SPDR;

	temp = (data>>24) & 0xff;
	SPDR = temp;
	while (!(SPSR & (1<<SPIF)));
	ret = ret << 8;
	ret |= SPDR;

	temp = (data>>16) & 0xff;
	SPDR = temp;
	while (!(SPSR & (1<<SPIF)));
	ret = ret << 8;
	ret |= SPDR;

	temp = (data>>8) & 0xff;
	SPDR = temp;
	while (!(SPSR & (1<<SPIF)));
	ret = ret << 8;
	ret |= SPDR;

	temp = (data>>0) & 0xff;
	SPDR = temp;
	while (!(SPSR & (1<<SPIF)));
	ret = ret << 8;
	ret |= SPDR;

	spi_status_reg = status;

	TMC_setup_chip_SPI(none);

	return ret;
}

void TMC_write_reg(enum TMC_ID chip, uint8_t addr, uint32_t data){
	SPI_transceive_reg(chip, (addr | 0x80), data);
}

uint32_t TMC_read_reg(enum TMC_ID chip, uint8_t addr){
	SPI_transceive_reg(chip, addr, 0x00000000);
	return SPI_transceive_reg(chip, addr, 0x00000000);
}

void TMC_minimal_setup(void){
	//TMC_write_reg(x, 0x6c, 0x000100c3);	// default (256 microsteps)
	TMC_write_reg(x, 0x10, 0x00061f0a);
	TMC_write_reg(x, 0x11, 0x0000000a);
	TMC_write_reg(x, 0x00, 0x00000004);
	TMC_write_reg(x, 0x13, 0x000001f4);
	TMC_write_reg(x, 0x6c, 0x140100c3);	// microstepping auf 16 (MRES bits 24-27 auf 0b0100, stepwidth = 2^MRES)
										// and enable microstep interpolation (microplier)
	TMC_write_reg(x, 0x0b, 0x0000005a);	// set GLOBAL_SCALER to 32/256 = 1/8 


	TMC_write_reg(y, 0x10, 0x00061f0a);
	TMC_write_reg(y, 0x11, 0x0000000a);
	TMC_write_reg(y, 0x00, 0x00000004);
	TMC_write_reg(y, 0x13, 0x000001f4);
	TMC_write_reg(y, 0x6c, 0x140100c3);	// microstepping auf 16 (MRES bits 24-27 auf 0b0100, stepwidth = 2^MRES)
										// and enable microstep interpolation (microplier)
	TMC_write_reg(y, 0x0b, 0x0000004e);	// set GLOBAL_SCALER to 32/256 = 1/8 



	TMC_write_reg(z, 0x10, 0x00061f0a);
	TMC_write_reg(z, 0x11, 0x0000000a);
	TMC_write_reg(z, 0x00, 0x00000004);
	TMC_write_reg(z, 0x13, 0x000001f4);
	TMC_write_reg(z, 0x6c, 0x140100c3);	// microstepping auf 16 (MRES bits 24-27 auf 0b0100, stepwidth = 2^MRES)
										// and enable microstep interpolation (microplier)
	TMC_write_reg(z, 0x0b, 0x0000004e);	// set GLOBAL_SCALER to 32/256 = 1/8 


}

void serial_init(long UBRR_VALUE){
	// Initialize USART
	UBRR0=UBRR_VALUE;			// set baud rate
	UCSR0B|=(1<<TXEN0);			// enable TX
	UCSR0B|=(1<<RXEN0);			// enable RX
	UCSR0B|=(1<<RXCIE0);			// RX complete interrupt
	UCSR0C|=(1<<UCSZ01)|(1<<UCSZ01); 	// no parity, 1 stop bit, 8-bit data
}

inline void serial_char(unsigned char data){
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = data;
}

void serial_break(void){
	serial_char(10); // LF
	serial_char(13); // CR
}

void serial_string(char* s) {
	while (*s != '\0')
		serial_char(*s++);
}

int main (void) {
	// declare needed variables for serial string concat
	char *buf;
	size_t stringsize;

	SPI_setup();
	//tmc5160_init();

	serial_init(9600);

	//union GCONF gconf_x;
	//gconf_x.recalibrate = 1;

	// Prints sizeof struct to compiler error
	//char checker(int);
	//char checkSizeOfInt[sizeof(GCONF)]={checker(&checkSizeOfInt)};
	
	_delay_ms(500);
	TMC_minimal_setup();

	//TMC_write_reg(x, 0x00, gconf_x.intRep);

	uint32_t mask;
	while(1){
		// get status register
		uint32_t status_x=TMC_read_reg(x, 0x6f);
		uint32_t status_y=TMC_read_reg(y, 0x6f);
		uint32_t status_z=TMC_read_reg(z, 0x6f);

		// get current bits (bits 16-20)
		mask = 0x1F0000;
		uint8_t current_x = (status_x & mask) >> 16;
		uint8_t current_y = (status_y & mask) >> 16;
		uint8_t current_z = (status_z & mask) >> 16;
		
		// get fullstep info (bit 15)
		mask = 0x8000;
		uint8_t fullstep_x = (status_x & mask) >> 15;
		uint8_t fullstep_y = (status_y & mask) >> 15;
		uint8_t fullstep_z = (status_z & mask) >> 15;

		// get Stallguard2 info (9 smallest bits)
		mask = 0x3ff;
		uint16_t sg_x = status_x & mask;
		uint16_t sg_z = status_z & mask;
		uint16_t sg_y = status_y & mask;
		

		// Current info
		// check needed size and convert serial string and numbers to char array
		stringsize = snprintf(NULL, 0, "Actual Current - X: %d\tY: %d\tZ: %d", current_x, current_y, current_z);
		buf = (char *)malloc(stringsize + 1);
		snprintf(buf, stringsize+1, "Actual Current - X: %d\tY: %d\tZ: %d", current_x, current_y, current_z);
		// serial print char array
		serial_string(buf);

		// Fullstep info
		// check needed size and convert serial string and numbers to char array
		stringsize = snprintf(NULL, 0, "Actual Fullstep - X: %dk\tY: %d\tZ: %d", fullstep_x, fullstep_y, fullstep_z);
		buf = (char *)malloc(stringsize + 1);
		snprintf(buf, stringsize+1, "Actual Fullstep - X: %d\tY: %d\tZ: %d", fullstep_x, fullstep_y, fullstep_z);
		// serial print char array
		serial_string(buf);

		// Stallguard Results
		// check needed size and convert serial string and numbers to char array
		stringsize = snprintf(NULL, 0, "Actual Stallguard - X: %l\tY: %l\tZ: %l", fullstep_x, fullstep_y, fullstep_z);
		buf = (char *)malloc(stringsize + 1);
		snprintf(buf, stringsize+1, "Actual Stallguard - X: %l\tY: %l\tZ: %l", fullstep_x, fullstep_y, fullstep_z);
		// serial print char array
		serial_string(buf);

		_delay_ms(100);
	}
}
