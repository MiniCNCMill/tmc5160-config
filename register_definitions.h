
// General Configuration Registers (0x00-0x0f)

//	Addr: 0x00	Access: RW
#define GCONF_MASK 0x0003ffff
typedef union GCONF{
	struct {
		uint8_t recalibrate: 1;
		uint8_t faststandstill: 1;
		uint8_t en_pwm_mode: 1;
		uint8_t multistep_filt: 1;
		uint8_t shaft: 1;
		uint8_t diag0_error: 1;
		uint8_t diag0_otpw: 1;
		uint8_t diag0_stall: 1;	//auch diag0_step
		uint8_t diag1_stall: 1;	//auch diag1_dir
		uint8_t diag1_index: 1;
		uint8_t diag1_onstate: 1;
		uint8_t diag1_steps_skipped: 1;
		uint8_t diag0_int_pushpull: 1;
		uint8_t diag1_poscomp_pushpull: 1;
		uint8_t small_hysteresis: 1;
		uint8_t stop_enable: 1;
		uint8_t direct_mode: 1;
		uint8_t test_mode: 1;
	};
	uint32_t intRep;
} GCONF;



//	Addr: 0x01	Access: R+WC (write-clear)
#define GSTAT_MASK 0x00000007
typedef union GSTAT{
	struct{
		uint8_t reset: 1;
		uint8_t drv_err: 1;
		uint8_t uv_cp: 1;
	};
	uint32_t intRep;
} GSTAT;



//	Addr: 0x02	Access: R
#define IFCNT_MASK 0x000000ff
typedef union IFCNT{
	struct{
		uint8_t IFCNT: 8;
	};
	uint32_t intRep;
} IFCNT;


//	Addr: 0x03	Access: W
#define SLAVECONF_MASK 0x00000fff
typedef union SLAVECONF{
	struct{
		uint8_t SLAVEADDR: 8;
		uint8_t SENDDELAY: 4;
	};
	uint32_t intRep;
} SLAVECONF;

//	Addr: 0x04	Access: R
#define IOIN_MASK 0xff0000ff
typedef union IOIN{
	struct {
		uint8_t REFL_STEP: 1;
		uint8_t REFR_DIR: 1;
		uint8_t ENCB_DCEN_CFG4: 1;
		uint8_t ENCA_DCIN_CFG5: 1;
		uint8_t DRV_ENN: 1;
		uint8_t ENC_N_DCO_CFG6: 1;
		uint8_t SD_MODE: 1;
		uint8_t SWCOMP_IN: 1;
		uint16_t gap_0: 16;	// Spacer, not used
		uint8_t VERSION: 8;
	};
	uint32_t intRep;
} IOIN;


//	Addr: 0x04	Access: W
#define OUTPUT_MASK 0x00000001
typedef union OUTPUT{
	struct {
		uint8_t OUTPUT: 1;
	};
	uint32_t intRep;
} OUTPUT;

//	Addr: 0x05	Access: W
#define X_COMPARE_MASK 0xffffffff
typedef union X_COMPARE{
	struct {
		uint32_t X_COMPARE: 32;
	};
	uint32_t intRep;
} X_COMPARE;

//	Addr: 0x06	Access: W
#define OTP_PROG_MASK 0x0000ff37
typedef union OTP_PROG{
	struct {
		uint8_t OTPBIT: 3;
		uint8_t gap_0: 1;	// Spacer, not used
		uint8_t OTPBYTE: 2;
		uint8_t gap_1: 3;	// Spacer, not used
		uint8_t OTPMAGIC: 8;
	};
    uint32_t intRep;
} OTP_PROG;

//	Addr: 0x07	Access: R
#define OTP_READ_MASK 0x000000ff
typedef union OTP_READ{
	struct {
		uint8_t OTP0: 8;
	};
    uint32_t intRep;
} OTP_READ;

//	Addr: 0x08	Access: RW
#define FACTORY_CONF_MASK 0x0000001f
typedef union FACTORY_CONF{
	struct {
		uint8_t FCLKTRIM: 5;
	};
    uint32_t intRep;
} FACTORY_CONF;

//	Addr: 0x09	Access: W
#define SHORT_CONF_MASK 0x00070f0f
typedef union SHORT_CONF{
	struct {
		uint8_t S2VS_LEVEL: 4;
		uint8_t gap_0: 4;	// Spacer, not used
		uint8_t S2G_LEVEL: 4;
		uint8_t gap_1: 4;	// Spacer, not used
		uint8_t SHORTFILTER: 2;
		uint8_t shortdelay: 1;
	};
    uint32_t intRep;
} SHORT_CONF;

//	Addr: 0x0a	Access: W
#define DRV_CONF_MASK 0x003f0f1f
typedef union DRV_CONF{
	struct {
		uint8_t BBMTIME: 5;
		uint8_t gap_0: 3;	// Spacer, not used
		uint8_t BBMCLKS: 4;
		uint8_t gap_1: 4;	// Spacer, not used
		uint8_t OTSELECT: 2;
		uint8_t DRVSTRENGTH: 2;
		uint8_t FILT_ISENSE: 2;
	};
    uint32_t intRep;
} DRV_CONF;

//	Addr: 0x0b	Access: W
#define GLOBAL_SCALER_MASK 0x000000ff
typedef union GLOBAL_SCALER{
	struct {
		uint8_t GLOBAL_SCALER: 8;
	};
    uint32_t intRep;
} GLOBAL_SCALER;

//	Addr: 0x0c	Access: R
#define OFFSET_READ_MASK 0x0000ffff
typedef union OFFSET_READ{
	struct {
		int8_t offset_phase_B: 8;	// signed (TODO: what standard?)
		int8_t offset_phase_A: 8;	// signed (TODO: what standard?)
	};
    uint32_t intRep;
} OFFSET_READ;

//	Addr: 0x10	Access: W
#define IHOLD_IRUN_MASK 0x00071f1f
typedef union IHOLD_IRUN{
	struct {
		uint8_t IHOLD: 5;
		uint8_t gap_2: 3;	// Spacer, not used
		uint8_t IRUN: 5;
		uint8_t gap_3: 3;	// Spacer, not used
		uint8_t IHOLDDELAY: 3;
	};
    uint32_t intRep;
} IHOLD_IRUN;

//	Addr: 0x11	Access: W
#define TPOWER_DOWN_MASK 0x000000ff
typedef union TPOWER_DOWN{
	struct {
		uint8_t TPOWERDOWN: 8;	// minimum of 2 is required to allow automatic tuning of StealthChop PWM_OFS_AUTO
	};
    uint32_t intRep;
} TPOWER_DOWN;

//	Addr: 0x12	Access: R
#define TSTEP_MASK 0x000fffff
typedef union TSTEP{
	struct {
		uint8_t TSTEP: 20; // Actual measured time between two 1/256 microsteps derived from the step input frequency in units of 1/fCLK. Measured value is (2^20)-1 in case of overflow or stand still.
	};
    uint32_t intRep;
} TSTEP;

//	Addr: 0x13	Access: W
#define TPWMTHRS_MASK 0x000fffff
typedef union TPWMTHRS{
	struct {
		uint8_t TPWMTHRS: 20;
	};
    uint32_t intRep;
} TPWMTHRS;

//	Addr: 0x14	Access: W
#define TCOOLTHRS_MASK 0x000fffff
typedef union TCOOLTHRS{
	struct {
		uint8_t TCOOLTHRS: 20;	// This is the lower threshold velocity for switching on smart energy CoolStep and StallGuard feature. (unsigned)
	};
    uint32_t intRep;
} TCOOLTHRS;

//	Addr: 0x15	Access: W
#define THIGH_MASK 0x000fffff
typedef union THIGH{
	struct {
		uint8_t THIGH: 20;	
	};
    uint32_t intRep;
} THIGH;

//
// Ramp Generator Registors
//	Addr: 0x20	Access: RW
#define RAMPMODE_MASK 0x00000003
typedef union RAMPMODE{
	struct {
		uint8_t RAMPMODE: 2;	
	};
    uint32_t intRep;
} RAMPMODE;

//	Addr: 0x21	Access: RW
#define XACTUAL_MASK 0xffffffff
typedef union XACTUAL{
	struct {
		int8_t XACTUAL: 32;	 // Actual motor position (signed)
	};
    uint32_t intRep;
} XACTUAL;

//	Addr: 0x22	Access: R
#define VACTUAL_MASK 0x00ffffff
typedef union VACTUAL{
	struct {
		int8_t VACTUAL: 24;	// Actual motor velocity from ramp generator (signed)
	};
    uint32_t intRep;
} VACTUAL;

//	Addr: 0x23	Access: W
#define VSTART_MASK 0x0002ffff
typedef union VSTART{
	struct {
		uint8_t VSTART: 18;	// Motor start velocity (unsigned)
	};
    uint32_t intRep;
} VSTART;

//	Addr: 0x24	Access: W
#define A1_MASK 0x0000ffff
typedef union A1{
	struct {
		uint8_t A1: 16;		// First acceleration between VSTART and V1 (unsigned)
	};
    uint32_t intRep;
} A1;

//	Addr: 0x25	Access: W
#define V1_MASK 0x000fffff
typedef union V1{
	struct {
		uint8_t V1: 20;		// First acceleration / deceleration phase threshold velocity (unsigned)
	};
    uint32_t intRep;
} V1;

//	Addr: 0x26	Access: W
#define AMAX_MASK 0x0000ffff
typedef union AMAX{
	struct {
		uint8_t AMAX: 16;	// Second acceleration between V1 and VMAX (unsigned)
	};
    uint32_t intRep;
} AMAX;

//	Addr: 0x27	Access: W
#define VMAX_MASK 0x007fffff
typedef union VMAX{
	struct {
		uint8_t VMAX: 23;	// Motion ramp target velocity (for positioning ensure VMAX ≥ VSTART) (unsigned)
	};
    uint32_t intRep;
} VMAX;

//	Addr: 0x28	Access: W
#define DMAX_MASK 0x0000ffff
typedef union DMAX{
	struct {
		uint8_t DMAX: 16;	// Deceleration between VMAX and V1 (unsigned)
	};
    uint32_t intRep;
} DMAX;

//	Addr: 0x2a	Access: W
#define D1_MASK 0x0000ffff
typedef union D1{
	struct {
		uint8_t D1: 16;		// Deceleration between V1 and VSTOP (unsigned)
	};
    uint32_t intRep;
} D1;

//	Addr: 0x2b	Access: W
#define VSTOP_MASK 0x0003ffff
typedef union VSTOP{
	struct {
		uint8_t VSTOP: 18;	// Motor stop velocity (unsigned)  - Attention: Do not set 0 in positioning mode, minimum 10 recommend!
	};
    uint32_t intRep;
} VSTOP;

//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;


/*
//	Addr: 0x	Access: W
#define _MASK 0x000fffff
typedef union {
	struct {
		uint8_t : 20;	
	};
    uint32_t intRep;
} ;
*/

// weiter ab Seite 38 im Datenblatt

/*
	uint8_t : 1;
	uint8_t : 1;
	uint8_t : 1;
	uint8_t : 1;
*/